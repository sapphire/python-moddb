#
# Regular cron jobs for the moddb package.
#
0 4	* * *	root	[ -x /usr/bin/moddb_maintenance ] && /usr/bin/moddb_maintenance
