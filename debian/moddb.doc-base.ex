Document: moddb
Title: Debian moddb Manual
Author: <insert document author here>
Abstract: This manual describes what moddb is
 and how it can be used to
 manage online manuals on Debian systems.
Section: unknown

Format: debiandoc-sgml
Files: /usr/share/doc/moddb/moddb.sgml.gz

Format: postscript
Files: /usr/share/doc/moddb/moddb.ps.gz

Format: text
Files: /usr/share/doc/moddb/moddb.text.gz

Format: HTML
Index: /usr/share/doc/moddb/html/index.html
Files: /usr/share/doc/moddb/html/*.html
